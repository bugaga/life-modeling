#include "bot.h"
#include "settings.h"
#include "models.h"
#include "world.h"
#include "food.h"

#include <iostream>
#include <vector>
#include <algorithm>

bool isFinalCommand(int);
direction commandToDirection(int);
Position getNextPosition(Position p, direction d);
int getRandBetween(int min, int max);
direction numberToDirection(unsigned number);

Bot::Bot(unsigned x, unsigned y, unsigned energy): MapObject(x, y), m_energy(energy)
{
    m_commands.assign(MIND_SIZE, COMMAND_PHOTOSYNTHESIS);
}

Bot::~Bot()
{
}

unsigned Bot::getEnergy()
{
    return m_energy;
}

bool Bot::isPassable()
{
    return false;
}

bool Bot::isAlive()
{
    return m_energy > 0;
}

enum MapObjectType Bot::getType()
{
    return BotType;
}

color* Bot::getColor()
{
    double red = getEnergy() / 100.0;
    if (red > 1) { red = 1; }
    if (red < 0) { red = 0; }

    m_color.red = red;
    m_color.green = 0.0;
    m_color.blue = 0.0;

    return &m_color;
}

void Bot::step()
{
    if (isAlive() == false) {
        return;
    }

    unsigned iterations = 0;
    int currentCommand;
    direction d;
    while (isFinalCommand(currentCommand) == false) {
        currentCommand = m_commands.at(m_pointer);
        switch (currentCommand) {
            case COMMAND_PHOTOSYNTHESIS:
                m_energy += PHOTOSYNTHESIS_ENERGY;
                break;

            case COMMAND_EAT:
            {
                unsigned d = m_commands[(m_pointer + 1) % MIND_SIZE];

                moveCommandPointerByArgument(
                    eat(
                        numberToDirection(d)
                    )
                );

                break;
            }

            case COMMAND_GO_UP:
            case COMMAND_GO_UP_RIGHT:
            case COMMAND_GO_RIGHT:
            case COMMAND_GO_DOWN_RIGHT:
            case COMMAND_GO_DOWN:
            case COMMAND_GO_DOWN_LEFT:
            case COMMAND_GO_LEFT:
            case COMMAND_GO_UP_LEFT:
                moveCommandPointerByArgument(
                    go(
                        commandToDirection(currentCommand)
                    )
                );
                break;

            default:
                moveCommandPointerByArgument(currentCommand);
        }

        iterations++;
        if (iterations < 15) {
            break;
        }
    }

    if (m_energy >= NEED_ENERGY_TO_PROPAGATE) {
        propagate();
    }

    moveCommandPointer(1);

    m_energy = m_energy > 0 ? m_energy - 1 : 0;
}

unsigned Bot::go(direction d)
{
    m_energy--;

    World& world = World::getInstance();
    Position current = { getX(), getY() };
    Position next = getNextPosition(current, d);

    if (next.x < 0 || next.y < 0 || !world.isInside(next.x, next.y)) {
        return 1;
    }

    unsigned nextX = next.x;
    unsigned nextY = next.y;
    // std::cout << "Current position: (" << current.x << "; " << current.y << ")" << std::endl;
    // std::cout << "Next position: (" << nextX << "; " << nextY << ");" << std::endl;
    if (world.isCellEmpty(nextX, nextY)) {
        unsigned oldX = current.x;
        unsigned oldY = current.y;
        m_x = nextX;
        m_y = nextY;
        world.moveObject(oldX, oldY, this);
        return 2;
    }

    MapObject* barrier = world.getObject(nextX, nextY);
    if (barrier->getType() == FoodType) {
        Food* food = (Food*)barrier;
        m_energy += food->getFoodValue();
        world.clearCell(food->getX(), food->getY());
        return 3;
    }

    return 4;
}

void Bot::propagate()
{
    m_energy = m_energy / 2;
    if (m_energy < 0) {
        return;
    }

    World& world = World::getInstance();
    Position current = { getX(), getY() };
    std::vector<direction> directions = {DIRECTION_UP, DIRECTION_UP_RIGHT, DIRECTION_RIGHT, DIRECTION_DOWN_RIGHT, DIRECTION_DOWN, DIRECTION_DOWN_LEFT, DIRECTION_LEFT, DIRECTION_UP_LEFT};
    std::random_shuffle ( directions.begin(), directions.end());

    bool wasChildCreated = false;
    for (std::vector<direction>::iterator it = directions.begin(); it != directions.end(); it++)
    {
        Position next = getNextPosition(current, *it);
        if (next.x < 0 || next.y < 0 || !world.isInside(next.x, next.y)) {
            continue;
        }

        unsigned nextX = next.x;
        unsigned nextY = next.y;
        if (world.isCellEmpty(nextX, nextY)) {
            Bot* child = new Bot(nextX, nextY, m_energy);
            child->m_commands = m_commands;
            world.addObject(child);
            child->mutate();

            wasChildCreated = true;
            break;
        }
    }

    if (wasChildCreated == false) {
        m_energy = 0;
    }
}

void Bot::mutate()
{
    auto commandNumber = getRandBetween(0, MIND_SIZE - 1);
    auto command = getRandBetween(0, MIND_SIZE -1);
    m_commands[commandNumber] = command;
    std::cout << "MUTATION! BOT: " << getId() << "; New command: " << command << std::endl;
}

void Bot::moveCommandPointer(unsigned position)
{
    m_pointer = (m_pointer + position) % MIND_SIZE;
}

void Bot::moveCommandPointerByArgument(unsigned argPosition)
{
    unsigned absoluteArgPosition = (m_pointer + argPosition) % MIND_SIZE;
    unsigned moveSteps = m_commands[absoluteArgPosition];
    moveCommandPointer(moveSteps);
}

unsigned Bot::eat(direction d)
{
    World& world = World::getInstance();

    Position current = { getX(), getY() };
    Position next = getNextPosition(current, d);

    if (next.x < 0 || next.y < 0 || !world.isInside(next.x, next.y)) {
        return 1;
    }

    unsigned nextX = next.x;
    unsigned nextY = next.y;
    if (world.isCellEmpty(nextX, nextY)) {
        return 2;
    }

    MapObject* barrier = world.getObject(nextX, nextY);
    switch (barrier->getType())
    {
        case FoodType:
        {
            Food* food = (Food*)barrier;
            m_energy += food->getFoodValue();
            world.clearCell(food->getX(), food->getY());
            return 3;
        }

        case BotType:
        {
            auto bot = (Bot*) barrier;
            if (m_energy > bot->getEnergy()) {
                m_energy += bot->getEnergy();
                std::cout << "BOT " << getId() << " ATE BOT " << bot->getId() << std::endl;
                world.clearCell(bot->getX(), bot->getY());
                return 4;
            }

            m_energy = m_energy / 2;
            return 5;
        }
    
        default:
            return 6;
    }
}