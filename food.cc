#include "food.h"
#include <iostream>

Food::Food(unsigned x, unsigned y, int foodValue):
    MapObject(x, y), m_food_value(foodValue)
{
    m_color.blue = 0.0;
    m_color.green = 1.0;
    m_color.red = 0.0;
}

bool Food::isPassable()
{
    return true;
}

enum MapObjectType Food::getType()
{
    return FoodType;
}

int Food::getFoodValue()
{
    return m_food_value;
}