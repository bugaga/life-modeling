#include <sys/time.h>
#include <chrono>
#include <iostream>

#include "settings.h"
#include "constants.h"
#include "models.h"

unsigned generateId()
{
	static unsigned long lastId = 0;
	return ++lastId;
}

bool isFinalCommand(int command)
{
	return 
		command == COMMAND_EAT ||
		(command >= COMMAND_GO_UP && command <= COMMAND_GO_UP_LEFT ) ||
		command == COMMAND_PHOTOSYNTHESIS ||
		command == COMMAND_PROPAGATE;
}

direction commandToDirection(int command)
{
	switch (command)
	{
		case COMMAND_GO_UP: 
			return DIRECTION_UP;

		case COMMAND_GO_UP_RIGHT:
			return DIRECTION_UP_RIGHT;

		case COMMAND_GO_RIGHT:
			return DIRECTION_RIGHT;

		case COMMAND_GO_DOWN_RIGHT:
			return DIRECTION_DOWN_RIGHT;

		case COMMAND_GO_DOWN:
			return DIRECTION_DOWN;

		case COMMAND_GO_DOWN_LEFT:
			return DIRECTION_DOWN_LEFT;

		case COMMAND_GO_LEFT:
			return DIRECTION_LEFT;

		case COMMAND_GO_UP_LEFT:
			return DIRECTION_UP_LEFT;

		default:
			std::cout << "commandToDirection: Cannot convert command '" << command << "' to direction!" << std::endl;
			throw "Uknown command";
	}
}

direction numberToDirection(unsigned number)
{
	unsigned n = number % 8; 
	switch (n)
	{
		case 0:
			return DIRECTION_UP;

		case 1:
			return DIRECTION_UP_RIGHT;

		case 2:
			return DIRECTION_RIGHT;

		case 3:
			return DIRECTION_DOWN_RIGHT;

		case 4:
			return DIRECTION_DOWN;

		case 5:
			return DIRECTION_DOWN_LEFT;

		case 6:
			return DIRECTION_LEFT;

		case 7:
			return DIRECTION_UP_LEFT;

		default:
			std::cout << "numberToDirection: cannot convert number '" << number << "' to direction!" << std::endl;
			throw "cannot convert number to direction";
	}
}

Position getNextPosition(Position p, direction d)
{
	Position result = p;
	switch (d)
	{
		case DIRECTION_UP:
			result.y--;
			break;

		case DIRECTION_UP_RIGHT:
			result.x++;
			result.y--;
			break;

		case DIRECTION_RIGHT:
			result.x++;
			break;

		case DIRECTION_DOWN_RIGHT:
			result.x++;
			result.y++;
			break;

		case DIRECTION_DOWN:
			result.y++;
			break;

		case DIRECTION_DOWN_LEFT:
			result.x--;
			result.y++;
			break;

		case DIRECTION_LEFT:
			result.x--;
			break;

		case DIRECTION_UP_LEFT:
			result.x--;
			result.y--;
			break;
	}

	return result;
}

long unsigned getTimestampMiliseconds()
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

int getRandBetween(int min, int max)
{
	long unsigned time, time2 = getTimestampMiliseconds();
	time2 = (time2/100) * 100;
	int pseudo_rand = time - time2;
	return time % (max - min) + min;
}
