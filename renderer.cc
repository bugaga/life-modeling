#include "renderer.h"
#include "settings.h"
#include "bot.h"
#include "color.h"
#include "food.h"
#include "world.h"
#include "map_object.h"

#include <ctime>
#include <cmath>
#include <cairomm/context.h>
#include <glibmm/main.h>
#include <iostream>
#include <vector>
#include <iterator>

long unsigned getTimestampMiliseconds();

Renderer::Renderer(): secondsToNextStep(500)
{
    Glib::signal_timeout().connect( sigc::mem_fun(*this, &Renderer::update), true );
    add_events(Gdk::BUTTON_PRESS_MASK);
}

Renderer::~Renderer()
{
}

bool Renderer::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
    World& world = World::getInstance();
    // cr->set_line_width(2.0);
    for (int x = 0; x < MAP_WIDTH; x++) {
        for (int y = 0; y < MAP_HEIGHT; y++) {
            cr->set_source_rgb(0.0, 0.0, 0.0);
            cr->rectangle(x * MAP_CELL_SIZE_PX, y * MAP_CELL_SIZE_PX, MAP_CELL_SIZE_PX - 1, MAP_CELL_SIZE_PX - 1);
            if (world.isCellEmpty(x, y)) {
                cr->stroke();
            } else {
                MapObject* object = world.getObject(x, y);
                cr->set_source_rgb(object->getColor()->red , object->getColor()->green, object->getColor()->blue);
                cr->fill();
            }
        }
    }

    return true;
}

bool Renderer::update()
{
    long unsigned now = getTimestampMiliseconds();
    if (now - lastStep < secondsToNextStep) {
        return true;
    }

    lastStep = now;
    World::getInstance().step();
    auto win = get_window();
    if (win)
    {
        Gdk::Rectangle r(0, 0, get_allocation().get_width(), get_allocation().get_height());
        win->invalidate_rect(r, false);
    }

    return true;
}

bool Renderer::on_button_press_event(GdkEventButton * event)
{
    World& world = World::getInstance();
    if (event->type == GDK_BUTTON_PRESS) {
        int x = event->x / MAP_CELL_SIZE_PX;
        int y = event->y / MAP_CELL_SIZE_PX;

        if (world.isCellEmpty(x, y)) {
            Food* f = new Food(x, y, 10);
            world.addObject(f);
        } else {
            MapObject* object = world.getObject(x, y);
            switch (object->getType())
            {
                case BotType:
                    Bot* bot = (Bot*) object;
                    std::cout << "BOT:" << std::endl
                        << "ID:     " << bot->getId() << std::endl
                        << "Energy: " << bot->getEnergy() << std::endl
                        << "Mind:   " << std::endl;

                    std::vector<unsigned> commands = bot->getCommands();
                    std::vector<unsigned>::iterator it;
                    for (it = commands.begin() ; it != commands.end(); ++it)
                    {
                        std::cout << ' ';
                        if(std::distance(commands.begin(), it) == bot->getPointer())
                        {
                            std::cout << '>';
                        }
                        std::cout << *it;
                    }
                    std::cout << std::endl;
                    break;
            }
        }
    }

    return Gtk::DrawingArea::on_button_press_event(event);
}