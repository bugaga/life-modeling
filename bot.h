#ifndef BOT_H
#define BOT_H

#include <vector>

#include "settings.h"
#include "map_object.h"
#include "color.h"
#include "constants.h"

class Bot: public MapObject
{
private:
    unsigned m_energy;
    std::vector<unsigned> m_commands;
    unsigned m_pointer = 0;

    unsigned go(direction);
    unsigned eat(direction);

    void propagate();
	void mutate();
public:
    Bot(unsigned x, unsigned y, unsigned energy);
    ~Bot();

    unsigned getEnergy();
    std::vector<unsigned> getCommands()
    {
        return m_commands;
    }
    unsigned getPointer()
    {
        return m_pointer;
    }

    void step();

    bool isPassable();
    bool isAlive();

    void moveCommandPointer(unsigned positions);
    void moveCommandPointerByArgument(unsigned argPosition);

    MapObjectType getType();

    color* getColor();
};

#endif
