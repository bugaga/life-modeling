FLAGS=`pkg-config gtkmm-3.0 --cflags --libs`

all: main.o renderer.o world.o bot.o utils.o food.o
	g++ main.o renderer.o world.o bot.o utils.o food.o -o bin/game $(FLAGS)

main.o: main.cc
	g++ -c main.cc -o main.o $(FLAGS)

utils.o: utils.cc
	g++ -c utils.cc -o utils.o

renderer.o: renderer.cc
	g++ -c renderer.cc -o renderer.o $(FLAGS)

world.o: world.cc
	g++ -c world.cc -o world.o

bot.o: bot.cc
	g++ -c bot.cc -o bot.o

food.o: food.cc
	g++ -c food.cc -o food.o

clean:
	rm bin/* && rm *.o
