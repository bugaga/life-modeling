#ifndef MAP_OBJECT_H
#define MAP_OBJECT_H

#include "color.h"

unsigned generateId();

enum MapObjectType {
    BotType, FoodType, WallType
};

class MapObject
{
private:
    unsigned m_id;

    MapObject()
    {}

protected:
    unsigned m_x;
    unsigned m_y;
    color m_color;

public:
    MapObject(unsigned x, unsigned y): m_x(x), m_y(y)
    {
        m_id = generateId();
        m_color.red = 0;
        m_color.green = 0;
        m_color.blue = 0;
    }

    ~MapObject()
    {}

    unsigned getId()
    {
        return m_id;
    }

    unsigned getX()
    {
        return m_x;
    }

    unsigned getY()
    {
        return m_y;
    }

    virtual bool isPassable() = 0;
    virtual enum MapObjectType getType() = 0;
    virtual color* getColor()
    {
        return &m_color;
    }
};

#endif