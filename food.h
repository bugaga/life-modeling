#if !defined(FOOD_H)
#define FOOD_H

#include "map_object.h"

class Food: public MapObject
{
private:
    int m_food_value;

public:
    Food(unsigned x, unsigned y, int foodValue);

    int getFoodValue();

    bool isPassable();
    MapObjectType getType();
};

#endif // FOOD_H
