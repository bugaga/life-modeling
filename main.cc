#include <gtkmm.h>
#include <string>
#include <iostream>

#include "settings.h"
#include "world.h"
#include "renderer.h"
#include "food.h"

int main(int argc, char *argv[])
{
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    World& world = World::getInstance();

    Bot* bot = new Bot(25, 20, 100);
    world.addObject(bot);

    Food* food = new Food(3, 5, 10);
    world.addObject(food);

    Gtk::Window win;
    win.set_default_size(MAP_WIDTH * MAP_CELL_SIZE_PX, MAP_HEIGHT * MAP_CELL_SIZE_PX);
    win.set_title("DrawingArea");

    Renderer area;
    win.add(area);
    area.show();

    return app->run(win);
}
