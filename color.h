#if !defined(COLOR_H)
#define COLOR_H

typedef struct
{
	double red;
	double green;
	double blue;
} color;

#endif // COLOR_H
