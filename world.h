#ifndef WORLD_H
#define WORLD_H

#include <vector>
#include <unordered_map>

#include "settings.h"
#include "map_object.h"
#include "bot.h"

typedef std::unordered_map<int, MapObject*> ObjectsMap;

class World
{
private:
    int map[MAP_WIDTH][MAP_HEIGHT];
    ObjectsMap m_objects;
    World();
    World(const World&);
    World& operator=(World&);

public:
    ~World();

    static World& getInstance();

    void step();
    void addObject(MapObject* object);
    MapObject* getObject(unsigned x, unsigned y);
    void moveObject(unsigned oldX, unsigned oldY, MapObject* object);
    bool isCellEmpty(unsigned x, unsigned y);
    bool isInside(unsigned x, unsigned y);
	void clearCell(unsigned x, unsigned y);
};

#endif // WORLD_H
