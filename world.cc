#include "exception.h"
#include "world.h"

#include <iostream>

World::World()
{
    for (int x = 0; x < MAP_WIDTH; x++) {
        for (int y = 0; y < MAP_HEIGHT; y++) {
            map[x][y] = -1;
        }
    }
}

World::~World()
{
    for (int x = 0; x < MAP_WIDTH; x++) {
        delete map[x];
    }

    delete map;
}

World& World::getInstance()
{
    static World instance;
    return instance;
}

void World::step()
{
    Bot* bot;
    for (ObjectsMap::iterator it=m_objects.begin(); it!=m_objects.end(); ++it) {
        // std::cout << "Type: " << it->second->getType() << "; on (" << it->second->getX() << "; " << it->second->getY() << ")" << std::endl;
        if (it->second->getType() == BotType) {
            bot = (Bot*)it->second;
            bot->step();
        }
    }
}

void World::addObject(MapObject* object)
{
    if (!isInside(object->getX(), object->getY())) {
        std::cout << "EXCRPTION: WORLD__ADD_OBJECT__SET_TO_OUTSIDE_MAP" << std::endl;
        throw WORLD__ADD_OBJECT__SET_TO_OUTSIDE_MAP;
    }

    if (!isCellEmpty(object->getX(), object->getY())) {
        std::cout << "EXCRPTION: WORLD__ADD_OBJECT__SET_TO_BEASY_CELL" << std::endl;
        throw WORLD__ADD_OBJECT__SET_TO_BEASY_CELL;
    }

    m_objects.emplace(object->getId(), object);
    map[object->getX()][object->getY()] = object->getId();
}

MapObject* World::getObject(unsigned x, unsigned y)
{
    if (isCellEmpty(x, y)) {
        throw WORLD__GET_OBJECT__GET_FROM_EMPTY_CELL;
    }

    return m_objects.at(map[x][y]);
}

void World::moveObject(unsigned oldX, unsigned oldY, MapObject* object)
{
    if (isCellEmpty(oldX, oldY)) {
        std::cout << "moveObject: Old postion is empty!";
        throw 1;
    }

    if (isCellEmpty(object->getX(), object->getY()) == false) {
        std::cout << "moveObject: new postiorion is not empty!";
        throw 1;
    }

    MapObject* obj = getObject(oldX, oldY);
    if (obj->getId() == object->getId()) {
        map[oldX][oldY] = -1;
        map[object->getX()][object->getY()] = object->getId();
    } else {
        std::cout << "moveObject: object at new position is not the same from argument!";
    }
}

bool World::isCellEmpty(unsigned x, unsigned y)
{
    return isInside(x, y) && map[x][y] == -1;
}

bool World::isInside(unsigned x, unsigned y)
{
    return x < MAP_WIDTH && y < MAP_HEIGHT;
}

void World::clearCell(unsigned x, unsigned y)
{
	if (!isCellEmpty(x, y)) {
		int position = map[x][y];
		m_objects.erase(position);
		map[x][y]=-1;
	}
}
