#if !defined(RENDERER_H)
#define RENDERER_H

#include <gtkmm/drawingarea.h>

class Renderer: public Gtk::DrawingArea
{
public:
    Renderer();
    virtual ~Renderer();
    unsigned secondsToNextStep;
    unsigned long lastStep;

protected:
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

    bool on_button_press_event(GdkEventButton * event) override;

    bool update();
};

#endif // RENDERER_H
